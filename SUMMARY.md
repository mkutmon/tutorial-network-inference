# Summary

* [Home](README.md)
* [Part A: Correlation](pages/correlation.md)
* [Part B: Regression](pages/regression.md)
* [Part C: Mutual information](pages/mutual-information.md)
* [Part D: Bayesian](pages/bayesian.md)
* [Part E: Comparing networks](pages/comparison.md)

