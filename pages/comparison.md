# Comparing networks

If you still have time, we would like to ask you to think about how you could "assess" the quality of the generated networks. How could you assess if a specific inferred network is better than another? Can this be scored somehow? 

Discuss with your peers! If you have time, you can try to implement a simple "scoring function".