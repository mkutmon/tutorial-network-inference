# Mutual information methods

Mutual information networks produce undirected networks. These methods quantify the degree of dependence of two genes. Using the [parmigene R package](https://doi.org/10.1093/bioinformatics/btr274) we can immediately use several of the mutual information methods including ARACNE, CLR and MRNET.

---

### Step 1: Install and load required packages

```
BiocManager::install(c("parmigene", "igraph", "RCy3"))
library(RCy3)
library(parmigene)
library(igraph)
```

---

### Step 2: Load expression data

* Change your working directory to the folder containing the dataset.
* Load the gene expression data as a data frame in R

```
data <- read.delim("insilico-data.txt", row.names=1)
```

---

### Step 3: Estimate mutual information (MI) between all matrix rows and run inference algorithms

The `knmi.all` function computes the MI between all pairs of rows of matrix `mat` using
entropy estimates from K-nearest neighbor distances. 

You can check the documentation on the individual MI methods [here](https://cran.r-project.org/web/packages/parmigene/parmigene.pdf). 

```
mi <- knnmi.all(data)
grn1 <- aracne.a(mi)
grn2 <- aracne.m(mi)
grn3 <- clr(mi)
grn4 <- mrnet(mi)
```

> Q1: How large are the created adjacency matrices? Do you see positive and negative values? How can you interpret this weight?

---

### Step 4: Visualize correlation network in Cytoscape

We can now create an igraph graph object and load it into Cytoscape to visualize the network. Compare it with the "true" network provided by the dream challenge.

```
graph1 <- igraph::graph_from_adjacency_matrix(grn1, weighted=TRUE)
createNetworkFromIgraph(graph1, title="ARACNE.A_Network",collection="MI")
setVisualStyle("default")

graph2 <- igraph::graph_from_adjacency_matrix(grn2, weighted=TRUE)
createNetworkFromIgraph(graph2, title="ARACNE.M_Network",collection="MI")
setVisualStyle("default")

graph3 <- igraph::graph_from_adjacency_matrix(grn3, weighted=TRUE)
createNetworkFromIgraph(graph3, title="CLR_Network",collection="MI")
setVisualStyle("default")

graph4 <- igraph::graph_from_adjacency_matrix(grn4, weighted=TRUE)
createNetworkFromIgraph(graph4, title="MRNET_Network",collection="MI")
setVisualStyle("default")
```

> Q2: How do the inferred networks look like? Do the different algorithms provide similar results? 

> Q3: How much of the 'true' network is present in the MI networks? Do these methods find links that are not "true" links? How many "true" links are missing?
