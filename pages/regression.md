# Regression networks

There are many different methods available. In this tutorial, we will use the [GENIE3 method](https://doi.org/10.1371/journal.pone.0012776) for regression based network inference.

Regression-based methods produce directed (causal) networks. 

---

### Step 1: Install and load required packages

```
BiocManager::install(c("GENIE3", "igraph", "RCy3"))
library(RCy3)
library(GENIE3)
library(igraph)
```

---

### Step 2: Load expression data

* Change your working directory to the folder containing the dataset.
* Load the gene expression data as a data frame in R

```
data <- read.delim("insilico-data.txt", row.names=1)
```

---

### Step 3: Calculate weighted adjacency matrix

The `GENIE3()` function takes as input argument a gene expression matrix `exprMatr`. Each row of that matrix must correspond to a gene and each column must correspond to a sample. The gene names must be specified in `rownames(exprMatr)`. The sample names can be specified in `colnames(exprMatr)`, but this is not mandatory.

```
set.seed(123) # for reproducibility of results
weightMat <- GENIE3(as.matrix(data))
```

This step might take a while - it is performing a regression problem for each gene individually. 

> Q1: What is the dimension of the weighted adjacency matrix? How can you interpret the weights in the matrix?

---

### Step 4: Filtering of weighted adjacency matrix

You can obtain the list of all the regulatory links (from most likely to least likely) with this command:
```
linkList <- getLinkList(weightMat)
```

The resulting `linkList` matrix contains the ranking of links. Each row corresponds to a regulatory link. The first column shows the regulator, the second column shows the target gene, and the last column indicates the weight of the link.
Note that the ranking that is obtained will be slightly different from one run to another. This is due to the intrinsic randomness of the Random Forest and Extra-Trees methods.

Usually, one is only interested in extracting the most likely regulatory links. The optional parameter report.max sets the number of top-ranked links to report or you can use a threshold on the weight:

```
linkList.max <- getLinkList(weightMat, reportMax=5)
linkList.t <- getLinkList(weightMat, threshold=0.1)
```

> Q2: Which type of filtering would you use? Why?

---

## Step 5: Visualize correlation network in Cytoscape

We can now create an igraph graph object and load it into Cytoscape to visualize the network. Compare it with the "true" network provided by the dream challenge. 

```
graph.max <- igraph::graph_from_data_frame(linkList.max, directed=TRUE, vertices=rownames(data))
createNetworkFromIgraph(graph.max, title="GENIE3_Network_Max",collection="GENIE3")
copyVisualStyle("default","GENIE3")
setVisualStyle("GENIE3")
setEdgeTargetArrowShapeDefault("Arrow", style.name = "GENIE3")

graph.t <- igraph::graph_from_data_frame(linkList.t, directed=TRUE, vertices=rownames(data))
createNetworkFromIgraph(graph.t, title="GENIE3_Network_Threshold",collection="GENIE3")
setVisualStyle("GENIE3")
```

> Q3: Try different thresholds/reportMax and compare the results.

> Q4: How much of the 'true' network is present in the correlation network? Did this approach found links that are not "true" links? How many "true" links are missing?
