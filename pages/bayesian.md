# Bayesian networks

There are many different tools available to infer Bayesian networks, e.g. [BNFinder2](http://bioputer.mimuw.edu.pl/software/bnf/) or the 
[GRENITS R package](http://www.bioconductor.org/packages/devel/bioc/vignettes/GRENITS/inst/doc/GRENITS_package.pdf). In this tutorial, we will use the GRENITS R package to analyse the DREAM challenge insilico data. 

Bayesian networks create directed (causal) probability networks. 

**Note**: The calculations can take quite long on a normal laptop (>30 min).

---

### Step 1: Install and load required packages

```
BiocManager::install(c("GRENITS", "igraph", "RCy3"))
library(GRENITS)
library(igraph)
library(RCy3)
```

---

### Step 2: Load expression data

* Change your working directory to the folder containing the dataset.
* Load the gene expression data as a data frame in R

```
data <- read.delim("insilico-data.txt", row.names=1)
```

---

### Step 3: Run Markov chain Monte Carlo (MCMC) function and analyze results

Define a local folder to store analysis output. 
`LinearNet` will run two MCMC chains with the default parameters. For non default
parameter values, a parameter vector can be provided (See `?mcmc.defaultParams_Linear`).
The output folder contains the raw output from the MCMC run (chain1 and
chain2), gene names (geneNames.txt) and the values of the parameters used
(runInfo.txt). Next we analyse the output.

```
output.folder <- ... 
LinearNet(output.folder, data)
analyse.output(output.folder)
```

Check covergence plots in output folder if the MCMC reached convergence. This is important before moving on!

>  Q1: Does the MCMC reach convergence? 

---

### Step 4: Load probability matrix

To predict a network we first load the inferred network probabilities

```
prob.file <- paste(output.folder, "/NetworkProbability_Matrix.txt", sep = "")
prob.mat <- read.table(prob.file)
prob.mat <- as.matrix(prob.mat)
```

>  Q2: How does the probability matrix look like? How can you interpret the values in the matrix?

---

### Step 5: Apply threshold

Check AnalysisPlots.pdf in output folder to see which threshold makes sense. 
As an example, all values below a threshold of 0.1 are set to 0. No link will be added in the network.

```
prob.mat[prob.mat < 0.1] <- 0
```

> Q3: Based on the AnalysisPlot.pdf which cut-off would you use? 
---

### Step 6: Visualize network in Cytoscape

We can now create an igraph graph object and load it into Cytoscape to visualize the network. Compare it with the "true" network provided by the dream challenge. 

```
graph <- igraph::graph_from_adjacency_matrix(prob.mat,weighted=TRUE)
createNetworkFromIgraph(graph, title="GRENITS",collection="Bayesian")
copyVisualStyle("default","GRENITS")
setVisualStyle("GRENITS")
setEdgeTargetArrowShapeDefault("Arrow", style.name = "GRENITS")
```

> Q4: Try different thresholds/reportMax and compare the results.

> Q5: How much of the 'true' network is present in the correlation network? Did this approach found links that are not "true" links? How many "true" links are missing?

