# Correlation networks

Correlation networks produce undirected networks. You select a correlation cut-off to decide if a link it present or not present in the network. 

Most common tool for gene co-expression analysis is [WGCNA](https://doi.org/10.1186/1471-2105-9-559) (weighted correlation network analysis) which is available as an R package.

>  Q1: Why can we not use WGCNA for the insilico example dataset from the DREAM challenge?

---

Using R, we can calculate the Paerson correlation between each gene pair and create an adjacency matrix to infer a correlation network. 

### Step 1: Install and load required packages

```
BiocManager::install(c("corrr", "RCy3", "igraph"))
library(RCy3)
library(corrr)
library(igraph)
```

---

### Step 2: Load expression data

* Change your working directory to the folder containing the dataset.
* Load the gene expression data as a data frame in R

```
data <- read.delim("insilico-data.txt", row.names=1)
```

>  Q2: How many genes and samples are in the dataset?

---

### Step 3: Calculate Paerson correlation

Using the corrr package, we can simply calculate the paerson correlation between all genes in the data frame. It requires samples as rows, so we first need to transpose the data frame.
```
res.cor <- correlate(t(data), diagonal = 0)
res.cor <- as.data.frame(res.cor)
row.names(res.cor) <- res.cor$term
res.cor[1] <- NULL
```

> Q3: How large is the created similarity [co-expression] matrix? Do you see positive and negative values? What does that mean?

---

### Step 4: Create adjacency matrix

Using a correlation threshold, we create a weighted adjacency matrix. Every value below the threshold will be set to 0 to indicate that no link between the two genes should be added. 

```
res.cor.filtered <- res.cor
res.cor.filtered[res.cor.filtered < 0.4] <- 0
```

> Q4: Do you see many zeros in the adjacency matrix?

---

### Step 5: Visualize correlation network in Cytoscape

We can now create an igraph graph object and load it into Cytoscape to visualize the network. Compare it with the "true" network provided by the dream challenge. Make sure you have the Cytoscape session open with the true network that you downloaded before!

```
graph <- igraph::graph_from_adjacency_matrix(as.matrix(res.cor.filtered), weighted=TRUE)
createNetworkFromIgraph(graph,title="Correlation_Network",collection="Correlation")
```

> Q5: Try out different correlation cutoffs. How does the network change?

> Q6: How much of the 'true' network is present in the correlation network? Did this approach found links that are not "true" links? How many "true" links are missing?
