# Network inference tutorial

* **Author:** Martina Summer-Kutmon, AIshwarya Iyer
* **Last updated:** 2024-09-10

---

### Example dataset used

For this tutorial, we will use the insilico example dataset from the HPN-DREAM breast cancer network inference challenge and we will try to infer the true network structure using four different network inference methods (correlation, regression, mutual information and bayesian). The aim is to learn the different tools and to assess the advantages and disadvantages of each methodology for infering small gene regulatory network (20 genes). 

* Check the description of the insilico data here: [https://www.synapse.org/#!Synapse:syn1720047/wiki/56061](https://www.synapse.org/#!Synapse:syn1720047/wiki/56061)
* For this tutorial, we only included timepoint 0-10.
* You can download the data (insilico-data.txt) from [here](https://gitlab.com/mkutmon/msb1014-skills2/raw/master/data/insilico-data.txt?inline=false).


The 'true' network structure looks like this (download Cytoscape session with solution from [here](https://gitlab.com/mkutmon/msb1014-skills2/raw/master/data/true-network.cys)).
![alt text](img/true-network.png "True Network")

### Getting started

Open the true network Cytoscape session before you start. Then all inferred networks will be added to this session and you can easily compare them with the true network using the merge functionality of Cytoscape. 

In the menu on the left side, you can get started by looking at the instructions of the correlation network inference method. 
